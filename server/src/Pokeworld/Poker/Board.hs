{-# LANGUAGE DeriveDataTypeable #-}

module Pokeworld.Poker.Board where

import Data.Data
import GHC.Stack

import Pokeworld.Poker.Cards

data Board
  = PreFlop
    !Deck
  | Flop
    !Card
    !Card
    !Card
    !Deck
  | Turn
    !Card
    !Card
    !Card
    !Card
    !Deck
  | River
    {-# UNPACK #-} !Hand
  | Done
    {-# UNPACK #-} !Hand
  deriving (Show, Data)

newBoard :: Int -> IO ([(Card, Card)], Board)
newBoard numOfPlayers = do
  deck <- randomDeck
  let
    (card1, deck')  = (take numOfPlayers deck, drop numOfPlayers deck)
    (card2, deck'') = (take numOfPlayers deck', drop numOfPlayers deck')
  pure (zip card1 card2, PreFlop deck'')


stepBoard :: HasCallStack => Board -> Board
stepBoard = \case
  PreFlop (_ : c1 : c2 : c3 : rest) -> Flop c1 c2 c3 rest

  Flop c1 c2 c3 (_ : c4 : rest) -> Turn c1 c2 c3 c4 rest

  Turn c1 c2 c3 c4 (_ : c5 : _) -> River (Hand c1 c2 c3 c4 c5)

  River h -> Done h

  Done h -> Done h

  _ -> error $ "Deck underflow before flop"

isDone :: Board -> Bool
isDone = \case
  Done{} -> True
  _ -> False

