{-# LANGUAGE OverloadedStrings, DeriveDataTypeable #-}

module Pokeworld.Poker.Round where

import Data.Maybe
import Data.Data
import qualified Data.Map as M
import qualified Data.List.NonEmpty as NEL
import qualified Data.Text as T

import Pokeworld.Poker.Board
import Pokeworld.Poker.Cards

data Round
  = Betting RoundBetState
  | Winners (Maybe HandType) (NEL.NonEmpty Player)
  deriving (Show, Data)

data RoundBetState
  = RoundBetState
  { rbsPot :: !Pot
  , rbsBetSize :: !Bet
  , rbsPlayers :: [PlayerId]
  , rbsPlayedPlayers :: [Player]
  , rbsToPlayPlayers :: [Player]
  , rbsBoardState :: Board
  }
  deriving (Show, Data)

newtype Pot = Pot { getPot :: Int }
  deriving (Show, Read, Eq, Data)

newtype Bet = Bet { getBet :: Int }
  deriving (Show, Read, Eq, Ord, Data)

newtype Chips = Chips { getChips :: Int }
  deriving (Show, Read, Data)

data BetType
  = Fold
  | Call
  | Raise Bet
  deriving (Show, Read, Eq, Data)

data Player
  = Player
  { pId :: PlayerId
  , pName :: T.Text
  , pChips :: Chips
  , pCards :: (Card, Card)
  , pInvestedTotal :: Bet
  , pInvestedBet :: Bet
  }
  deriving (Show, Data)

newtype PlayerId = PlayerId { getId :: Int }
  deriving (Show, Eq, Ord, Data)

type SmallBlind = Bet

newPlayer :: PlayerId -> Chips -> (Card, Card) -> Player
newPlayer pid chips hand = Player pid "buck" chips hand (Bet 0) (Bet 0)

testRound :: IO Round
testRound = newRound (Bet 10) (PlayerId 1, Chips 100) (PlayerId 2, Chips 100) []

newRound :: SmallBlind -> (PlayerId, Chips) -> (PlayerId, Chips) -> [(PlayerId, Chips)] -> IO Round
newRound blind p1 p2 rest = do
  (cards, board) <- newBoard (length rest + 2)
  let players = zipWith (uncurry newPlayer) (p1 : p2 : rest) cards
  pure
    . stepRound (Raise $ Bet $ getBet blind * 2)
    . stepRound (Raise blind)
    . Betting
    $ RoundBetState
    { rbsPot = Pot 0
    , rbsBetSize = Bet 0
    , rbsPlayers = map pId players
    , rbsPlayedPlayers = []
    , rbsToPlayPlayers = players
    , rbsBoardState = board
    }

checkRoundDone :: Round -> Maybe (Maybe HandType, NEL.NonEmpty Player)
checkRoundDone = \case
  Winners h w -> Just (h, w)
  Betting RoundBetState{ rbsBoardState = Done board, rbsToPlayPlayers, rbsPot } ->
    let
      (hand, map fst -> winners) = winningPlayers board rbsToPlayPlayers
    in Just
      ( Just hand
      , NEL.fromList
        . map (\p@Player{pChips=Chips cs} -> p { pChips = Chips $ cs + (getPot rbsPot `div` length winners) })
        $ winners
      )
  _ -> Nothing


stepRound :: BetType -> Round -> Round
stepRound bet = \case
  Betting RoundBetState{ rbsBoardState = Done board, rbsToPlayPlayers, rbsPot } ->
    let
      (hand, map fst -> winners) = winningPlayers board rbsToPlayPlayers
    in
      Winners (Just hand)
      . NEL.fromList
      . map (\p@Player{pChips=Chips cs} -> p { pChips = Chips $ cs + (getPot rbsPot `div` length winners) })
      $ winners

  Betting rbs@RoundBetState{..} ->
    case (rbsPlayedPlayers, rbsToPlayPlayers) of
      ([], []) -> error "Unexpected state - no players in round"
      (_, []) ->
          Betting $
            rbs
              { rbsPlayedPlayers = []
              , rbsToPlayPlayers =
                seatPlayers rbsPlayers rbsPlayedPlayers
              , rbsBoardState = stepBoard rbsBoardState
              , rbsBetSize = Bet 0
              }

      ([], [w@Player{pChips=Chips cs}]) ->
        Winners Nothing
        . NEL.fromList
        $ [w { pChips = Chips $ cs + getPot rbsPot}]

      ([w@Player{pChips=Chips cs}], [_])
        | bet == Fold ->
          Winners Nothing
          . NEL.fromList
          $ [w { pChips = Chips $ cs + getPot rbsPot}]

      (played, player : rest) -> case (bet, played, rest) of
        (_, [w@Player{pChips=Chips cs}], [])
          | bet == Fold ->
            Winners Nothing
            . NEL.fromList
            $ [w { pChips = Chips $ cs + getPot rbsPot}]

        (_, [], [w@Player{pChips=Chips cs}])
          | bet == Fold ->
            Winners Nothing
            . NEL.fromList
            $ [w { pChips = Chips $ cs + getPot rbsPot}]

        (Fold, _, []) ->
          Betting $
            rbs
              { rbsPlayedPlayers = []
              , rbsToPlayPlayers = rest
              , rbsBoardState = stepBoard rbsBoardState
              , rbsBetSize = Bet 0
              }

        (Fold, _, _) ->
          Betting $
            rbs
              { rbsToPlayPlayers = rest
              }

        (Call, _, []) ->
          Betting $
            rbs
              { rbsPlayedPlayers = []
              , rbsToPlayPlayers =
                seatPlayers rbsPlayers
                . (:) (snd $ playerCall player rbsBetSize)
                $ rbsPlayedPlayers
              , rbsBoardState = stepBoard rbsBoardState
              , rbsBetSize = Bet 0
              , rbsPot = Pot $ getPot rbsPot + getBet (fst $ playerCall player rbsBetSize)
              }

        (Call, _, _) ->
          Betting $
            rbs
              { rbsPlayedPlayers = snd (playerCall player rbsBetSize) : rbsPlayedPlayers
              , rbsToPlayPlayers = rest
              , rbsPot = Pot $ getPot rbsPot + getBet (fst $ playerCall player rbsBetSize)
              }

        (Raise r, _, _)
          | r > rbsBetSize ->
            Betting $
              rbs
                { rbsPlayedPlayers = [snd $ playerCall player r]
                , rbsToPlayPlayers =
                  rest ++ reverse rbsPlayedPlayers
                , rbsBetSize = r
                , rbsPot = Pot $ getPot rbsPot + getBet (fst $ playerCall player r)
                }

        -- same as call
        (Raise{}, _, []) ->
          Betting $
            rbs
              { rbsPlayedPlayers = []
              , rbsToPlayPlayers =
                seatPlayers rbsPlayers
                . (:) (snd $ playerCall player rbsBetSize)
                $ rbsPlayedPlayers
              , rbsBoardState = stepBoard rbsBoardState
              , rbsBetSize = Bet 0
              , rbsPot = Pot $ getPot rbsPot + getBet (fst $ playerCall player rbsBetSize)
              }

        -- same as call
        (Raise{}, _, _) ->
          Betting $
            rbs
              { rbsPlayedPlayers = snd (playerCall player rbsBetSize) : rbsPlayedPlayers
              , rbsToPlayPlayers = rest
              , rbsPot = Pot $ getPot rbsPot + getBet (fst $ playerCall player rbsBetSize)
              }

  w@Winners{} -> w

seatPlayers :: [PlayerId] -> [Player] -> [Player]
seatPlayers players playedPlayers =
  (\m -> mapMaybe (`M.lookup` m) players)
  . M.fromList
  . map (\p -> (pId p, p { pInvestedBet = Bet 0 }))
  $ playedPlayers

playerCall :: Player -> Bet -> (Bet, Player)
playerCall p bsize =
  let
    usedChips = min (getChips $ pChips p) (getBet bsize - getBet (pInvestedBet p))
  in
    (Bet usedChips,) $ p
      { pChips = Chips $ getChips (pChips p) - usedChips
      , pInvestedTotal = Bet $ getBet (pInvestedTotal p) + usedChips
      , pInvestedBet = Bet $ getBet (pInvestedBet p) + usedChips
      }

nextPlayer :: Round -> Maybe Player
nextPlayer = \case
  Betting RoundBetState{..} ->
    Just $ head rbsToPlayPlayers
  Winners{} -> Nothing

winningPlayers :: Hand -> [Player] -> (HandType, [(Player, Hand)])
winningPlayers board players =
  let
    bests = M.fromListWith (++) $
      map
        (\p -> case bestHand board $ pCards p of
            (htype, h) -> (htype, [(p, h)])
        )
        players
  in
    M.findMax bests

