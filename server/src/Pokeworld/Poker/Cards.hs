{-# LANGUAGE DeriveDataTypeable #-}

module Pokeworld.Poker.Cards where

import qualified Data.Vector as V
import qualified Data.Set as S
import qualified Data.Map as M
import VectorShuffling.Immutable (shuffle)
import System.Random
import GHC.Stack
import Data.List (sortOn, (\\))
import Data.List.Extra (groupOn)
import Text.Groom
import Data.Data

data Card
  = Card
  { cardSuit :: !CardSuit
  , cardRank :: !CardRank
  }
  deriving (Eq, Bounded, Data)

instance Show Card where
  show Card{..} =
    show cardRank ++ '_' : show cardSuit

data CardSuit
  = Spade
  | Club
  | Heart
  | Diamond
  deriving (Eq, Enum, Bounded, Data)

instance Show CardSuit where
  show = \case
    Spade -> "S"
    Club -> "C"
    Heart -> "H"
    Diamond -> "D"

data CardRank
  = Two
  | Three
  | Four
  | Five
  | Six
  | Seven
  | Eight
  | Nine
  | Ten
  | Jack
  | Queen
  | King
  | Ace
  deriving (Eq, Ord, Enum, Bounded, Data)

instance Show CardRank where
  show = \case
    Two -> "2"
    Three -> "3"
    Four -> "4"
    Five -> "5"
    Six -> "6"
    Seven -> "7"
    Eight -> "8"
    Nine -> "9"
    Ten -> "10"
    Jack -> "J"
    Queen -> "Q"
    King -> "K"
    Ace -> "A"


type Deck = [Card]

allCards :: [Card]
allCards =
  Card
    <$> [minBound..maxBound]
    <*> [minBound..maxBound]

randomDeck :: IO Deck
randomDeck =
  V.toList <$> getStdRandom (shuffle $ V.fromList allCards)

data Hand
  = Hand
  { hCard1 :: {-# UNPACK #-} !Card
  , hCard2 :: {-# UNPACK #-} !Card
  , hCard3 :: {-# UNPACK #-} !Card
  , hCard4 :: {-# UNPACK #-} !Card
  , hCard5 :: {-# UNPACK #-} !Card
  }
  deriving (Eq, Data)

instance Show Hand where
  show = show . toList

data HandType
  = HighCard {-# UNPACK #-} !Hand
  | Pair {-# UNPACK #-} !(Card, Card) {-# UNPACK #-} !Card {-# UNPACK #-} !Card {-# UNPACK #-} !Card
  | TwoPairs {-# UNPACK #-} !(Card, Card) {-# UNPACK #-} !(Card, Card) {-# UNPACK #-} !Card
  | Set {-# UNPACK #-} !(CardRank, CardSuit) {-# UNPACK #-} !(Card, Card)
  | Straight {-# UNPACK #-} !Hand
  | Flush {-# UNPACK #-} !Hand
  | FullHouse {-# UNPACK #-} !(CardRank, CardSuit) {-# UNPACK #-} !(CardRank, CardSuit, CardSuit)
  | FourOfAKind !CardRank !Card
  | StraightFlush {-# UNPACK #-} !Hand
  deriving (Show, Eq, Data)

instance Ord HandType where
  compare hand1 hand2 =
    case compare (rankHandType hand1) (rankHandType hand2) of
      EQ -> case (hand1, hand2) of
        (HighCard h1, HighCard h2) ->
          compare (map cardRank $ toList h1) (map cardRank $ toList h2)
        (Pair (p1,_) c1_3 c1_4 c1_5, Pair (p2,_) c2_3 c2_4 c2_5) ->
          case (uc (cardRank p1, cardRank p2), uc (cardRank c1_3, cardRank c2_3), uc (cardRank c1_4, cardRank c2_4), uc (cardRank c1_5, cardRank c2_5)) of
            (EQ, EQ, EQ, r) -> r
            (EQ, EQ, r, _) -> r
            (EQ, r, _, _) -> r
            (r, _, _, _) -> r

        (TwoPairs (p1_1, _) (p1_2, _) k1, TwoPairs (p2_1, _) (p2_2, _) k2) ->
          case (uc (cardRank p1_1, cardRank p2_1), uc (cardRank p1_2, cardRank p2_2), uc (cardRank k1, cardRank k2)) of
            (EQ, EQ, r) -> r
            (EQ, r, _) -> r
            (r, _, _) -> r

        (Set (s1, _) (k1_1, k1_2), Set (s2, _) (k2_1, k2_2)) ->
          case (uc (s1, s2), uc (cardRank k1_1, cardRank k2_1), uc (cardRank k1_2, cardRank k2_2)) of
            (EQ, EQ, r) -> r
            (EQ, r, _) -> r
            (r, _, _) -> r

        (Straight Hand{hCard1=h1}, Straight Hand{hCard1=h2}) ->
          uc (cardRank h1, cardRank h2)

        (Flush Hand{hCard1=h1}, Flush Hand{hCard1=h2}) ->
          uc (cardRank h1, cardRank h2)

        (FullHouse (s1, _) (p1, _, _), FullHouse (s2, _) (p2, _, _)) ->
          case (uc (s1, s2), uc (p1, p2)) of
            (EQ, r) -> r
            (r, _) -> r

        (FourOfAKind f1 k1, FourOfAKind f2 k2) ->
          case (uc (f1, f2), uc (cardRank k1, cardRank k2)) of
            (EQ, r) -> r
            (r, _) -> r

        (StraightFlush Hand{hCard1=h1}, StraightFlush Hand{hCard1=h2}) ->
          uc (cardRank h1, cardRank h2)

        hs -> error $ "Unexpected non-equal hand types: " ++ groom hs

      r -> r
    where
      uc = uncurry compare
      rankHandType = \case
        HighCard{} -> 0
        Pair{} -> 1
        TwoPairs{} -> 2
        Set{} -> 3
        FullHouse{} -> 4
        FourOfAKind{} -> 5
        Straight{} -> 6
        Flush{} -> 7
        StraightFlush{} -> 8
    
toList :: Hand -> [Card]
toList Hand{..} =
  [ hCard1
  , hCard2
  , hCard3
  , hCard4
  , hCard5
  ]

toHand :: HasCallStack => [Card] -> Hand
toHand [c1,c2,c3,c4,c5] = Hand c1 c2 c3 c4 c5
toHand pat = error $ "Expected list of length 5. Got: " ++ show pat

handType :: Hand -> HandType
handType (toList -> sortOn cardRank -> reverse -> groupOn cardRank -> sortOn length -> handPattern) = case handPattern of

  [[c], [Card{cardRank},_,_,_]] -> FourOfAKind cardRank c

  [[small1, small2], set@[Card{cardRank=bigRank},_,_]] ->
    FullHouse
      (bigRank, head $ [minBound..maxBound] \\ map cardSuit set)
      (cardRank small1, cardSuit small1, cardSuit small2)

  [[c1], [c2], set@[Card{cardRank},_,_]] ->
    Set (cardRank, head $ [minBound..maxBound] \\ map cardSuit set)  (c1, c2)

  [[kicker], [big1, big2], [small1, small2]] ->
    TwoPairs
      (big1, big2)
      (small1, small2)
      kicker

  [[c3], [c4], [c5], [pair1, pair2]] ->
    Pair
      (pair1, pair2) c3 c4 c5

  [[c1],[c2],[c3],[c4],[c5]] ->
    if
      | isSet && isStraight -> StraightFlush hand
      | isSet -> Flush hand
      | isStraight -> Straight hand
      | otherwise -> HighCard hand
    where
      flattened = [c1,c2,c3,c4,c5]
      hand
        | isStraight && cardRank c1 == Ace = toHand [c2,c3,c4,c5,c1]
        | otherwise = toHand flattened
      isSet = length (groupOn cardSuit flattened) == 1
      isStraight
        | cardRank c1 == Ace =
          let
            s = S.fromList $ map cardRank [c2,c3,c4,c5]
          in
            all (flip S.member s) [Two, Three, Four, Five]
        | otherwise =
          let
            s = S.fromList $ map cardRank [c1,c2,c3,c4]
          in
            all (flip S.member s . succ . cardRank) flattened

  _ -> error $ "Unexpected handType list pattern for Hand: " ++ show handPattern

pickHands :: Hand -> (Card, Card) -> [(HandType, Hand)]
pickHands (toList -> hand') (self1, self2) = do
  let hand = self1 : self2 : hand'
  pick1 <- hand
  pick2 <- hand \\ [pick1]
  pick3 <- hand \\ [pick1, pick2]
  pick4 <- hand \\ [pick1, pick2, pick3]
  pick5 <- hand \\ [pick1, pick2, pick3, pick4]
  let h = Hand pick1 pick2 pick3 pick4 pick5
  pure (handType h, h)

bestHand :: Hand -> (Card, Card) -> (HandType, Hand)
bestHand hand self =
  M.findMax $ M.fromList $ pickHands hand self
