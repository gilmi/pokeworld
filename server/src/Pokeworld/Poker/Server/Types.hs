{-# LANGUAGE OverloadedStrings, DeriveDataTypeable #-}

-- | Types for the system
module Pokeworld.Poker.Server.Types
  where

import Prelude hiding (round)
import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.Text.Encoding as T
import qualified Data.List.NonEmpty as NEL
import Data.Aeson hiding (Error)
import Data.Data
import Data.Generics.Uniplate.Data

import qualified Pokeworld.Poker.Round as R
import Pokeworld.Poker.Round hiding (Player)
import Pokeworld.Poker.Board
import Pokeworld.Poker.Cards

import Network.Socket
import Network.WebSockets (Connection)
import qualified Data.Map as M
import qualified Control.Concurrent.STM as STM

data Sock
  = NetSock Socket
  | WebSock Connection

data PlayerState
  = PlayerInGame R.Player
  | PlayerResting RestingPlayer
  deriving (Show, Data)

data RestingPlayer
  = RestingPlayer
  { pId :: PlayerId
  , pName :: T.Text
  , pChips :: Chips
  }
  deriving (Show, Data)

data PlayerComs
  = PlayerComs
  { _pId :: PlayerId
  , _pcOutQueue :: !(STM.TQueue MsgToPlayer)
  , _pcSocket :: Sock
  }

type PlayersComs = M.Map PlayerId PlayerComs
type Players = M.Map PlayerId PlayerState

data Server = Server
  { sPlayers :: Players
  , sPlayersComs :: PlayersComs
  , sRound :: Maybe Round
  , sPidVar :: STM.TVar Int
  , sLogger :: STM.TQueue T.Text
  , sRoundCmds :: STM.TQueue RoundCmd
  }

type ServerVar = STM.TVar Server

data RoundCmd
  = PlayerBetCmd PlayerId BetType

toLog :: STM.TVar Server -> T.Text -> IO ()
toLog server msg = STM.atomically $ do
  l <- sLogger <$> STM.readTVar server
  STM.writeTQueue l msg


type Name = T.Text

data CommandFromPlayer
  = SendMessage T.Text
  | BetCmd BetType
  | Quit
  deriving (Show, Read, Eq, Data)

data MsgToPlayer
  = GotMessage Message
  | PlayerJoined RestingPlayer
  | PlayerParted PlayerState
  | ErrorMsg ErrorMsg
  | RoundUpdate (Maybe (R.Player, BetType)) Round
  | DealtCards (Card, Card)
  | YourTurn
  | Welcome Players (Maybe Round)
  deriving (Show, Data)

data ErrorMsg
  = Error T.Text
  | InvalidCommand BS.ByteString
  | NotYourTurn
  deriving (Show, Eq, Ord, Data)

data Message = Message
  { _mPlayer :: PlayerState
  , _mText :: T.Text
  }
  deriving (Show, Data) -- , NFData)


instance ToJSON CommandFromPlayer where
  toJSON obj =
    object
      . (++) ["msg_type" .= showConstr (toConstr obj)]
      $ case obj of
        SendMessage msg ->
          [ "message" .= toJSON msg
          ]
        BetCmd cmd ->
          [ "bet_type" .= toJSON cmd
          ]
        Quit -> []

instance ToJSON BetType where
  toJSON obj =
    object
      . (++) ["type" .= showConstr (toConstr obj)]
      $ case obj of
        Raise b ->
          [ "bet" .= toJSON (getBet b)
          ]
        _ -> []

instance ToJSON MsgToPlayer where
  toJSON obj =
    object
      . (++) ["msg_type" .= showConstr (toConstr obj)]
      $ case obj of
        GotMessage msg ->
          [ "message" .= toJSON msg
          ]
        PlayerJoined player ->
          [ "player" .= toJSON player
          ]
        PlayerParted player ->
          [ "player" .= toJSON player
          ]
        DealtCards (card1, card2) ->
          [ "card1" .= toJSON card1
          , "card2" .= toJSON card2
          ]
        RoundUpdate _ round ->
          [ "round" .= toJSON round
          ]
        YourTurn ->
          []
        Welcome players mayRound ->
          [ "players" .= fmap toJSON (M.elems players)
          ] ++
          [ "round" .= toJSON round
          | Just round <- [mayRound]
          ]
        ErrorMsg err ->
          ["error" .= toJSON err
          ]

instance ToJSON R.Player where
  toJSON R.Player{..} =
    object
      [ "chips" .= getChips pChips
      , "name" .= String pName
      ]

instance ToJSON RestingPlayer where
  toJSON RestingPlayer{..} =
    object
      [ "chips" .= getChips pChips
      , "name" .= String pName
      ]

instance ToJSON PlayerState where
  toJSON = \case
    PlayerInGame p -> toJSON p
    PlayerResting p -> toJSON p

instance ToJSON Round where
  toJSON = \case
    Betting rbs ->
      object
        [ "round" .= String "betting"
        , "rbs" .= toJSON rbs
        ]
    Winners _ winners ->
      object
        [ "round" .= String "winners"
        , "winners" .=
          map
            ( \R.Player{..} -> object
              [ "name" .= pName
              , "card1" .= fst pCards
              , "card2" .= snd pCards
              ]
            )
            (NEL.toList winners)
        ]

instance ToJSON RoundBetState where
  toJSON RoundBetState{..} =
    object
      [ "pot" .= getPot rbsPot
      , "bet-size" .= getBet rbsBetSize
      , "board" .= toJSON rbsBoardState
      ]

instance ToJSON Board where
  toJSON board =
    object
      [ "state" .= String (T.pack $ showConstr $ toConstr board)
      , "cards" .= map (String . T.pack . show) [ c | c <- childrenBi board :: [Card] ]
      ]

instance ToJSON Card where
  toJSON Card{..} =
    object
      [ "rank" .= String (T.pack $ show cardRank)
      , "suit" .= String (T.pack $ show cardSuit)
      ]

instance ToJSON Message where
  toJSON msg =
    object
      [ "name" .= _mPlayer msg
      , "message" .= _mText msg
      ]

instance ToJSON ErrorMsg where
  toJSON = \case
    Error err ->
      object
        [ "type" .= String "Error"
        , "error" .= String err
        ]

    InvalidCommand err ->
      object
        [ "type" .= String "InvalidCommand"
        , "error" .= T.decodeUtf8 err
        ]
