{-# LANGUAGE OverloadedStrings #-}

module Pokeworld.Poker.Server.Run where

import Pokeworld.Poker.Server.Types
import Pokeworld.Poker.Server.Socks
import Pokeworld.Poker.Round (PlayerId(..), Chips(..), stepRound, nextPlayer, Player(..))

import Control.Exception
import Control.Monad
import Network.Socket
import qualified Network.WebSockets as WS
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Control.Concurrent.STM as STM
import qualified Control.Concurrent.Async as Async
import Control.Concurrent (forkFinally)

run :: IO ()
run = do
  server <- newServer
  Async.race_
    (runLogger server)
    $ Async.race_
        (roundCmdsHandler server)
        $ Async.race_
            (netSocketServer server)
            (webSocketServer server)

webSocketServer :: ServerVar -> IO ()
webSocketServer server = do
  toLog server "Waiting for web connections..."
  void $ WS.runServer "0.0.0.0" 8888 $ \pending -> do
    conn <- WS.acceptRequest pending
    WS.forkPingThread conn 120
    listener (WebSock conn) server

netSocketServer :: ServerVar -> IO ()
netSocketServer server = do
  addrInfos <-
    getAddrInfo
    (Just defaultHints{addrFlags = [AI_PASSIVE]})
    Nothing
    (Just "7777")
  let serverAddr = head addrInfos
  bracket
    (socket (addrFamily serverAddr) Stream defaultProtocol)
    (const (toLog server "\nClosed") <=< close) $ \sock -> do
      setSocketOption sock ReuseAddr 1
      bind sock (addrAddress serverAddr)
      listen sock 1
      toLog server "Waiting for connections..."
      void $ forever $ do
        (soc, _) <- accept sock
        forkFinally
          (listener (NetSock soc) server)
          (const $ close soc)

listener :: Sock -> ServerVar -> IO ()
listener sock serverVar = do
  bracket
    (STM.atomically $ do
      server <- STM.readTVar serverVar
      let
        nvTVar = sPidVar server
      nv <- STM.readTVar nvTVar
      STM.modifyTVar nvTVar (+1)
      oq <- STM.newTQueue
      let
        name = "Guest" <> T.pack (show nv)
        newPlayer = RestingPlayer
          { pId = PlayerId nv
          , pName = name
          , pChips = Chips 1000
          }
        newPlayerComs = PlayerComs
          { _pcOutQueue = oq
          , _pcSocket = sock
          , _pId = PlayerId nv
          }
      oq `STM.writeTQueue` Welcome (sPlayers server) (sRound server)
      STM.modifyTVar serverVar $ \s ->
        s
          { sPlayers =
            M.insert (PlayerId nv) (PlayerResting newPlayer) $ sPlayers s
          , sPlayersComs =
            M.insert (PlayerId nv) newPlayerComs $ sPlayersComs s
          }
      pure newPlayerComs
    )
    (const (toLog serverVar "\nClosed Player") <=< playerQuit serverVar) $ \coms -> do
      Async.race_
        (sendToPlayer serverVar coms)
        (receiveFromPlayer serverVar coms)

newServer :: IO ServerVar
newServer = do
  nv <- STM.newTVarIO 0
  l  <- STM.newTQueueIO
  q  <- STM.newTQueueIO
  STM.newTVarIO $
    Server
      { sPlayers = mempty
      , sPlayersComs = mempty
      , sPidVar = nv
      , sLogger = l
      , sRoundCmds = q
      , sRound = Nothing
      }

runLogger :: STM.TVar Server -> IO ()
runLogger server = forever $ do
  msg <- STM.atomically $ do
    l <- sLogger <$> STM.readTVar server
    STM.readTQueue l
  T.putStrLn msg


roundCmdsHandler :: ServerVar -> IO ()
roundCmdsHandler serverVar = forever $ STM.atomically $ do
  server <- STM.readTVar serverVar
  PlayerBetCmd pid bet <- STM.readTQueue $ sRoundCmds server
  case (sRound server, M.lookup pid (sPlayers server), M.lookup pid (sPlayersComs server)) of
    (Nothing, _, Just coms) ->
      _pcOutQueue coms `STM.writeTQueue` ErrorMsg NotYourTurn

    (Just round, _, Just coms)
      | Nothing <- nextPlayer round ->
        _pcOutQueue coms `STM.writeTQueue` ErrorMsg NotYourTurn

      | Just Player{pId=npid} <- nextPlayer round
      , npid /= pid ->
        _pcOutQueue coms `STM.writeTQueue` ErrorMsg NotYourTurn

    (Just round, Just (PlayerInGame Player{..}), _) ->
      STM.modifyTVar serverVar $ \server' ->
        server'
          { sRound = Just $ stepRound bet round
          }

    _ -> pure ()
