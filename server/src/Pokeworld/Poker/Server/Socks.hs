{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DisambiguateRecordFields #-}

module Pokeworld.Poker.Server.Socks
  where

import Pokeworld.Poker.Server.Types

import Prelude hiding (round)
import Data.Aeson (toEncoding)
import Data.Aeson.Encoding (encodingToLazyByteString)
import Data.Foldable
import Control.Arrow
import Control.Monad
import Control.Exception
import Network.WebSockets (receiveData, sendTextData, fromLazyByteString)
import Network.Socket (close)
import Network.Socket.ByteString
import qualified Data.Map as M
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as BS
import qualified Control.Concurrent.STM as STM
import qualified Network.Socket.ByteString as Net

sendToPlayer :: ServerVar -> PlayerComs -> IO ()
sendToPlayer serverVar player = forever $ do
  msg <- STM.atomically $ STM.readTQueue (_pcOutQueue player)
  case _pcSocket player of
    WebSock c -> do
      let
        bsMsg :: BS.ByteString
        bsMsg =
          fromLazyByteString
          . encodingToLazyByteString
          . toEncoding
          $ msg
      sendTextData c bsMsg
    NetSock s -> do
      let bsMsg = BS.pack (show msg) <> "\n"
      Net.sendAll s bsMsg
  toLog serverVar $
    T.pack $ show (_pId player) ++ " << " ++ show msg

receiveFromPlayer :: ServerVar -> PlayerComs -> IO ()
receiveFromPlayer serverVar player = do
  msg <- case _pcSocket player of
    NetSock s -> recv s 4096
    WebSock c -> receiveData c

  case parseCommand msg of
    Nothing -> do
      STM.atomically $
        _pcOutQueue player `STM.writeTQueue` ErrorMsg (InvalidCommand msg)
      receiveFromPlayer serverVar player

    Just cmd -> do
      toLog serverVar $
        T.pack $ show (_pId player) ++ " >> " ++ show msg
      stop <-
        catch
          (const False <$> act player serverVar cmd)
          (\(SomeException _) -> pure True)
      unless stop $
        receiveFromPlayer serverVar player

parseCommand :: BS.ByteString -> Maybe CommandFromPlayer
parseCommand = BS.unpack >>> reads >>> \case
  [(cmd,"")] -> pure cmd
  [(cmd,"\r")] -> pure cmd
  [(cmd,"\n")] -> pure cmd
  [(cmd,"\r\n")] -> pure cmd
  _ -> Nothing

act :: PlayerComs -> ServerVar -> CommandFromPlayer -> IO ()
act coms serverVar = \case
  SendMessage msg -> STM.atomically $ do
    server <- STM.readTVar serverVar
    case _pId coms `M.lookup` sPlayers server of
      Nothing -> do
        _pcOutQueue coms `STM.writeTQueue` ErrorMsg (Error "Player not found in players list")
      Just player ->
        for_ (sPlayersComs server)
          $ (`STM.writeTQueue` GotMessage (Message player msg))
          . _pcOutQueue

  BetCmd bet -> STM.atomically $ do
    server <- STM.readTVar serverVar
    sRoundCmds server `STM.writeTQueue` PlayerBetCmd (_pId coms) bet

  Quit -> playerQuit serverVar coms

playerQuit :: STM.TVar Server -> PlayerComs -> IO ()
playerQuit serverVar coms = do
  STM.atomically $ do
    STM.readTVar serverVar >>= \server ->
      case _pId coms `M.lookup` sPlayers server of
        Nothing -> pure ()
        Just ps ->
          for_ (sPlayersComs server)
            $ ((`STM.writeTQueue` PlayerParted ps) . _pcOutQueue)

    STM.modifyTVar serverVar $ \server ->
      server
        { sPlayers = M.delete (_pId coms) $ sPlayers server
        , sPlayersComs = M.delete (_pId coms) $ sPlayersComs server
        }

  case _pcSocket coms of
    NetSock s -> close s
    WebSock _ -> pure ()


