module Pokeworld.Run where

import Data.Char
import Prelude hiding (round)
import System.IO (hSetBuffering, stdout, stdin, BufferMode(NoBuffering))
import Text.Groom
import Text.Read

import Pokeworld.Poker.Round

run :: IO ()
run = do
  round <- testRound
  hSetBuffering stdin NoBuffering
  hSetBuffering stdout NoBuffering
  go round

go :: Round -> IO ()
go round = do
  case checkRoundDone round of
    Just (h, w) ->
      putStrLn $ groom $ Winners h w

    Nothing -> do
      putStrLn $ groom round
      putStrLn ""
      let player = (\p -> [show $ getId $ pId p, show $ pCards p]) <$> nextPlayer round
      putStr $ maybe "" unwords player
      putStr "> "
      line <- words . map toLower <$> getLine
      putStrLn ""
      case line of
        ["raise", n] | Just bet <- readMaybe n -> do
          let round' = stepRound (Raise $ Bet bet) round
          go round'

        [c] | c `elem` ["call", "check"] -> do
          let round' = stepRound Call round
          go round'

        ["fold"] -> do
          let round' = stepRound Fold round
          go round'

        _ -> do
          putStrLn "Invalid Input"
          go round




